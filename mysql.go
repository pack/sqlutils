package sqlutils

import (
	"fmt"
	"sort"
	"strings"
)

// MysqlOptions represent the database connection parameters.
// Note: Parameter map defaults to parseTime=true.
type MysqlOptions struct {
	Username string
	Password string
	Host     string
	Port     int
	Database string

	Parameters map[string]string // Mysql Connection paramters like parseTime=true
}

// ConnectionString is the connection string to use when connecting to mysql.
func (m *MysqlOptions) ConnectionString() string {
	var userConn string

	if m.Username == "" {
		m.Username = "root"
	}

	if m.Password != "" {
		userConn = strings.Join([]string{m.Username, m.Password}, ":")
	} else {
		userConn = m.Username
	}

	if m.Host == "" {
		m.Host = "127.0.0.1"
	}

	if m.Port == 0 {
		m.Port = 3306
	}

	return fmt.Sprintf(
		"%s@tcp(%s:%d)/%s%s",
		userConn,
		m.Host, m.Port,
		m.Database,
		m.sortedConnectParams(),
	)
}

// Returns sorted parameters key-values for the connection string.
func (m *MysqlOptions) sortedConnectParams() string {
	if len(m.Parameters) == 0 {
		return "?parseTime=true"
	}

	pLen := len(m.Parameters)
	params := make([]string, pLen)

	i := 0
	for k := range m.Parameters {
		params[i] = k
		i++
	}
	sort.Strings(params)

	kvParams := make([]string, pLen)
	for i, v := range params {
		kvParams[i] = fmt.Sprintf("%s=%s", v, m.Parameters[v])
	}
	return fmt.Sprintf("?%s", strings.Join(kvParams, "&"))
}
