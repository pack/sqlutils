package sqlutils

import (
	"fmt"
	"testing"
)

var TestMysqlDatabase = "testdb"

func TestMysqlConnectionString(t *testing.T) {
	t.Parallel()

	var connStringTests = []struct {
		opts     *MysqlOptions
		expected string
	}{
		{&MysqlOptions{
			Database: TestMysqlDatabase,
		}, fmt.Sprintf("root@tcp(127.0.0.1:3306)/%s?parseTime=true", TestMysqlDatabase)},
		{&MysqlOptions{
			Database: TestMysqlDatabase,
			Password: "testing",
		}, fmt.Sprintf("root:testing@tcp(127.0.0.1:3306)/%s?parseTime=true", TestMysqlDatabase)},
	}

	for _, tt := range connStringTests {
		actual := tt.opts.ConnectionString()
		if actual != tt.expected {
			t.Errorf("Mysql Connection String Incorrect: expected: `%s`, actual: `%s`", tt.expected, actual)
		}
	}
}
